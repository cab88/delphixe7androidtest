unit Unit8;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Ani, FMX.DateTimeCtrls, FMX.ListBox,
  FMX.Layouts, FMX.Memo;

type
  TForm8 = class(TForm)
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    ImageControl1: TImageControl;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    DateEdit1: TDateEdit;
    ComboBox1: TComboBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ComboBox2: TComboBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure DateEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ImageControl1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
		{ Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.fmx}
{$R *.LgXhdpiPh.fmx ANDROID}
{$R *.iPhone4in.fmx IOS}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TForm8.Button1Click(Sender: TObject);
begin
	Label1.Text:='hello '+Edit1.Text+'!';
end;

procedure TForm8.ComboBox1Change(Sender: TObject);
begin
	Memo1.Lines.Insert(0,(Format('选中%s的第%d项: %s.',
	[combobox1.Name,combobox1.Index,combobox1.Selected.Text])));
end;

procedure TForm8.DateEdit1Change(Sender: TObject);
begin
 ShowMessage(FormatDateTime('dddddd',dateEdit1.Date));
end;

procedure TForm8.FormCreate(Sender: TObject);
begin
	 ComboBox2.Items.Clear;
	 
	 ComboBox2.Items.Add('小兔');
	 ComboBox2.Items.Add('小鸡');
	 ComboBox2.Items.Add('小狗');
	 ComboBox2.Items.Add('小牛');
	 ComboBox2.Items.Add('小明');
		ComboBox1.ItemIndex:=2;
	 ComboBox2.ItemIndex:=ComboBox2.Items.IndexOf('小牛');
end;

procedure TForm8.ImageControl1Click(Sender: TObject);
begin
	 ShowMessage('我是一个带图片的按钮');
end;

procedure TForm8.SpeedButton1Click(Sender: TObject);
begin
	Memo1.Lines.Insert(0,(Format('选中%s按钮.',[speedbutton1.Text])));
end;

procedure TForm8.SpeedButton2Click(Sender: TObject);
begin
Memo1.Lines.Insert(0,(Format('选中%s按钮.',[speedbutton2.Text])));
end;

procedure TForm8.SpeedButton3Click(Sender: TObject);
begin
Memo1.Lines.Insert(0,(Format('选中%s按钮.',[speedbutton3.Text])));
end;

end.
