unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FMX.ListView.Types, FMX.ListView, FMX.StdCtrls, Data.Bind.Components,
  Data.Bind.DBScope, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt,System.IOUtils, Fmx.Bind.DBEngExt, FireDAC.FMXUI.Wait, FireDAC.Comp.UI;

type
  TForm1 = class(TForm)
    FDConnection1: TFDConnection;
    FDQueryCreateTable: TFDQuery;
    ListView1: TListView;
    BindSourceDB1: TBindSourceDB;
    FDQuery1: TFDQuery;
    LinkFillControlToFieldShopItem: TLinkFillControlToField;
    BindingsList1: TBindingsList;
    FDQueryInsert: TFDQuery;
    FDQueryDelete: TFDQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDQueryDeleteall: TFDQuery;
    ToolBar1: TToolBar;
    ButtonAdd: TButton;
    ButtonDelete: TButton;
    Label1: TLabel;

    procedure ListView1ItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure ButtonAddClick(Sender: TObject);
   
    procedure FDConnection1BeforeConnect(Sender: TObject);
    procedure FDConnection1AfterConnect(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure ToolBar1Click(Sender: TObject);

  private
		procedure OnInputQuery_Close(const AResult:TModalResult;const AValues:array of string);
		
  public
    { Public declarations }
  end;

var
	Form1: TForm1;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}
{$R *.LgXhdpiPh.fmx ANDROID}



procedure TForm1.ButtonAddClick(Sender: TObject);
	var
	Values: array[0..0] of string;
begin
	Values[0]:=string.Empty;
	InputQuery('�����¼�¼',['Name'],Values,self.OnInputQuery_Close);
end;

procedure TForm1.ButtonDeleteClick(Sender: TObject);
	var
	TaskName:string;
begin
	TaskName:=TListViewItem(ListView1.Selected).text;
	try
		FDQueryDelete.ParamByName('ShopItem').asstring:=TaskName;
		FDQueryDelete.ExecSQL();
		FDQuery1.close;
		FDQuery1.open;
		ButtonDelete.Visible:=ListView1.Selected<>nil;
	except on E: Exception do
		ShowMessage(e.Message);
	end;
end;



procedure TForm1.FDConnection1AfterConnect(Sender: TObject);
begin
	FDConnection1.ExecSQL('CREATE TABLE IF NOT EXISTS Item (ShopItem TEXT NOT NULL)');
end;

procedure TForm1.FDConnection1BeforeConnect(Sender: TObject);
begin
{$IF DEFINED(IOS) or DEFINED(ANDROID)}
	FDConnection1.Params.Values['Database']:=
	Tpath.Combine(Tpath.GetDocumentsPath,'shoplist.s3db');
{$ENDIF}	
end;

procedure TForm1.ListView1ItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
	ButtonDelete.Visible:=ListView1.Selected<>nil;
end;

procedure TForm1.OnInputQuery_Close(const AResult: TModalResult;
  const AValues: array of string);
 var
	TaskName:String;
 begin
	 TaskName:=string.Empty;
	 if AResult <> mrOk then Exit;
	 TaskName:=AValues[0];
	 try
		 if TaskName.Trim <> '' then
		 begin
			 FDQueryInsert.ParamByName('ShopItem').AsString:=TaskName;
			 FDQueryInsert.ExecSQL();
			 FDQuery1.Close;
			 FDQuery1.Open;
			 ButtonDelete.Visible:=ListView1.Selected<> nil;
		 end;
		 
	 except 
		on E: Exception do
		begin
		  ShowMessage(e.Message);
		end;
	 end;
				
 end;




procedure TForm1.ToolBar1Click(Sender: TObject);
begin
	try

		FDQueryDeleteall.ExecSQL();
		FDQuery1.close;
		FDQuery1.open;
		ButtonDelete.Visible:=ListView1.Selected<>nil;
	except on E: Exception do
		ShowMessage(e.Message);
	end;
end;

//procedure TForm1.ToolBar1Click(Sender: TObject);
//begin

//end;

end.
